<?php
namespace App\View\Helper;
 
use Cake\View\Helper;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
// use Cake\Network\Email\Email;
use Cake\Datasource\ConnectionManager; // For Custom query
use Cake\Mailer\Email;
use Cake\Core\Configure;
 
class CustomHelper extends Helper {

    public function getTargetLimit($manager_id){    	
    	$PlanModel = TableRegistry::get("user_plans");
  		$PlanData = $PlanModel->find('all')->where(['user_id'=> $manager_id, 'is_expired'=> 0])->toArray();
  		if(count($PlanData) > 0){
  			$str = "";
  			foreach ($PlanData as $plan) {
  				$str .= $plan['product_name'] ." / ". $plan['target_limit'] . "<br/>";
  			}
  			return $str;
  		}else{
  			return "-";
  		}
    }

    public function getLastExpirePlan($manager_id){      
      $PlanModel = TableRegistry::get("user_plans");
      $PlanData = $PlanModel->find('all')->select(['product_name', 'target_limit'])->where(['user_id'=> $manager_id, 'is_expired'=> 1])->order(['expiry_date'=> 'desc'])->first();
      if(count($PlanData) > 0){
        return $PlanData['product_name'] ." / ". $PlanData['target_limit'];
      }else{
        return "-";
      }
    }

    public function getLastlive($group_id){      
      $PlanModel = TableRegistry::get("live_streams");
      $PlanData = $PlanModel->find('all')->select(['stream_started'])->where(['group_id'=> $group_id])->order(['stream_started'=> 'desc'])->first();
     // return $group_id;
      if (!empty($PlanData['stream_started'])) {
        return  date("Y-m-d", strtotime($PlanData['stream_started']));
      } else {
        return "-";
      }
      // return "=====-";
    }
public function getRegisteredSocial($group_id){      
      $PlanModel = TableRegistry::get("comming_users");
      $PlanData = $PlanModel->find('all')->where(['group_id'=> $group_id])->count();
     // return $group_id;
      if (!empty($PlanData)) {
        return  $PlanData;
      } else {
        return "-";
      }
      // return "=====-";
    }

    public function getIsPAL($manager_id){      
      $PlanModel = TableRegistry::get("user_plans");
      $PlanData = $PlanModel->find('all')->select(['product_name', 'auto_limit'])->where(['user_id'=> $manager_id])->order(['expiry_date'=> 'desc'])->first();
      if(count($PlanData) > 0){
        return ($PlanData['auto_limit']) ? __(yes) : __(no) ;
      }else{
        return __(no);
      }
    }
    
    //function to check web registration acccess permission according manager id
    public function checkWebRegistrationAccess($manager_id){
        $applicationModel = TableRegistry::get("applications");
        $applicationData = $applicationModel->find('all')->select(['id_web_registration_enabled'])->where(['manager_id'=> $manager_id, 'id_web_registration_enabled'=> 1])->first();
        if(count($applicationData) > 0){
            return true;
        }else{
            return false;
        }
    }

    //function to return url according to url and module
    public function returnUrlAccordingToRoleAndModule($module,$menu,$auth){
        if($module=="web_registration"){
            $userId=$auth['id'];
            $applicationModel = TableRegistry::get("applications");
            $applications  = $applicationModel->find('all')->where(['manager_id' =>$userId])->first();
            if($menu=="form_listing"){
                if($auth['role']==1 || $auth['role']==5){
                    return HTTP_ROOT . 'webregistration/formlisting';
                }else{
                    if(!empty($applications)){
                        $application_id=$applications->id;
                        return HTTP_ROOT . 'webregistration/edit-form/'.$application_id;
                    }else {
                        return "#";
                    }
                }
            }else if($menu=="user_listing"){
                if($auth['role']==1 || $auth['role']==5) {
                    return HTTP_ROOT . 'webregistration/userlisting';
                }else{
                    if(!empty($applications)){
                        $application_id=$applications->id;
                        return HTTP_ROOT . 'webregistration/view-users/'.$application_id;
                    }else {
                        return "#";
                    }
                }
			}else if($menu=="video_gallery"){
				// OTHER ROLES VIEW!
				// if($auth['role']==1 || $auth['role']==5)
				if(!empty($applications)){
					$application_id=$applications->id;
					return HTTP_ROOT . 'webregistration/video-gallery/'.$application_id;
				}else {
					return "#";
				}
            }
			else if($menu=="golive"){
                    if(!empty($applications)){
                        $application_id=$applications->id;
                        return HTTP_ROOT . 'webregistration/golive/'.$application_id;
                    }else {
                        return "#";
                    }
              
            }
        }
    }

    public function getExpiryDates($manager_id){  
      $PlanModel = TableRegistry::get("user_plans");
      $PlanData = $PlanModel->find('all')->where(['user_id'=> $manager_id, 'is_expired'=> 0])->toArray();
      if(count($PlanData) > 0){
        $str = "";
        foreach ($PlanData as $plan) {
          $str .= date("Y-m-d", strtotime($plan['expiry_date'])). "<br/>";
        }
        return $str;
      }else{
        return "-";
      }
    }

	    //function to get form id according to user id
    function getFormIdByUserId($auth){
        $userId=$auth['id'];
        if (isset($auth['role']) && in_array( $auth['role'], ['4']) ) {
          $userId=$auth['parent_id'];
        }
        $FormListingModel = TableRegistry::get("form_listings");
        //$formListing  = $FormListingModel->find('all')->where(['owner_id'=> $userId])->contain(['CommingUsers','user_created','user_owner'])->order(['form_id' => 'DESC'])->toArray();
        $formListing  = $FormListingModel->find('all')->where(['owner_id'=> $userId])->order(['form_id' => 'DESC'])->toArray();
        if(!empty($formListing)){
            return $formListing[0]->form_id;
        }else{
            return false;
        }
    }

     function getTargetErrorUsers($formId){
        if($formId){
        $CommingUserListingModel = TableRegistry::get("comming_users");
        $CommingUserData1 = $CommingUserListingModel->find('all')->where(['form_id' => $formId, 'wowza_stream_status' => 'Error'])->toArray();
       
        if(!empty($CommingUserData1)){
          if( count($CommingUserData1) > 0){
              return true;
          }else{
              return false;
          }
        }else{
            return false;
        }
      }
    }

    public function getLiveBroadcastCount($group_id){    	
    	$LiveStreamsModel = TableRegistry::get("live_streams");
  		$totalCount = $LiveStreamsModel->find('all')->where(['group_id' => $group_id])->count();
  		return $totalCount;
    }

    public function getBroadcastReached($formId = 0, $duration = 0,$streamID=0){
      $SQL = "SELECT IFNULL(SUM(user.followers),0) as Reached from broadcast_details as detail LEFT JOIN `comming_users` as user ON detail.target_id = user.id";
      //$where = " where detail.status != 'UNPUBLISHED'";      
      if($duration != 0){
        $date =  date( 'Y-m-d', strtotime( date('Y-m-d') . ' -'.$duration.' day' ) );
        $where .= " AND detail.updated_date >= ". $date;
      }
      if($streamID != 0){
        $where .= " AND detail.stream_id = ". $streamID;
      }
      if($formId != 0){
        $where .= " AND detail.form_id = ". $formId;
      }
      $SQL .= $where;
      
      $conn = ConnectionManager::get('default');
      $broadcastResults = $conn->execute( $SQL )->fetch('assoc');
      return $broadcastResults['Reached'];
    }

    public function getFakeUsers(){
      $SQL = "SELECT * from fake_user";
      $conn = ConnectionManager::get('default');
      $FakeUsersResults = $conn->execute( $SQL )->fetch('assoc');
      return $FakeUsersResults;
    }

    public function getBroadCastData($formId = 0,$targetId = 0,$duration = 0,$streamId = 0,$broadcastID= 0){
    	
      $broadCastDetailsModel = TableRegistry::get('broadcast_details');
      $query = $broadCastDetailsModel->find(); 
      $query
      ->select([
        'vlike' => $query->func()->sum('vlike'),
        'view' => $query->func()->sum('view'),
        'comment' => $query->func()->sum('comment'),
        'love' => $query->func()->sum('love'),
        'none' => $query->func()->sum('none'),
        'wow' => $query->func()->sum('wow'),
        'pride' => $query->func()->sum('pride'),
        'thankful' => $query->func()->sum('thankful'),
        'angry' => $query->func()->sum('angry'),
        'sad' => $query->func()->sum('sad'),
        'haha' => $query->func()->sum('haha'),
        'dislike' => $query->func()->sum('dislike'),
        'share' => $query->func()->sum('share'),
        ]);
        if($duration != 0 ){
          $date =  date( 'Y-m-d', strtotime( date('Y-m-d') . ' -'.$duration.' day' ) );
          $details = $query->where(['updated_date > ' => $date]);
        }
       if($formId != 0 && $targetId != 0){
        $details = $query->where(['form_id' => $formId,'target_id' => $targetId])->first();
       }else if($targetId != 0 && $streamId != 0){
        $details = $query->where(['target_id' => $targetId, 'stream_id' => $streamId])->first();
       }else if($formId != 0){
        $details = $query->where(['form_id' => $formId])->first();
       }else if($targetId != 0){
        $details = $query->where(['target_id' => $targetId])->first();
       }else if($streamId != 0){
        $details = $query->where(['stream_id' => $streamId])->first();
       }else if($broadcastID != 0){
        $details = $query->where(['broadcast_id' => $broadcastID])->first();
       }else{
        $details = $query->first();
       }
      return $details; 
  }

  /*
    Method : getExpiredUsersInteractions()
    Title : Used to get expired users interactions 
    author : Sandhya Ghatoda
    Created Date : 18-11-2019
  */
  public function getExpiredUsersInteractions(){
      
      $expiredModel = TableRegistry::get("expired_user_interactions");
      $data = $expiredModel->find('all')->where($where)->first();

      return $data; 
  }

  public function getLastBroadCastData($targetId = 0){      
      $broadCastDetailsModel = TableRegistry::get('broadcast_details');
      $details = $broadCastDetailsModel->find()->where(['target_id' => $targetId])->order(['updated_date' => 'DESC'])->first();
      return $details; 
  }

  public function checkTargetsEndTime($identifier, $type, $access_token){
    //$url = "https://graph.facebook.com/v2.8/".$identifier."/?access_token=".$access_token;
    $url = FB_GRAPH_API_HOST . $identifier."/?access_token=".$access_token;
    $result = json_decode($this->getCurl($url));

    if(!empty($result->end_time)){
      if(strtotime($result->end_time) >= strtotime(date('Y-m-d H:i:s'))){
        return true;
      }else{
        return false;
      }
    }else{
      if(strtotime($result->start_time) >= strtotime(date('Y-m-d H:i:s'))){
        return true;
      }else{
        return false;
      }
    }
  }

  function getCurl($url){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array("cache-control: no-cache"),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    return $response;    
  }

  public function getVideoStatus($target_id, $stream_id){
    $broadCastDetailsModel = TableRegistry::get('broadcast_details');
    $details = $broadCastDetailsModel->find()->select(['broadcast_details.status', 'broadcast_details.message'])->where(['target_id'=> $target_id, 'stream_id'=> $stream_id])->first();
    return $details;
  }

  public function getBroadcastFollowers($stream_id){
    $CommingUsersModel = TableRegistry::get("comming_users");
    $BroadcastDetailModel = TableRegistry::get("broadcast_details");
    $targets = $BroadcastDetailModel->find()->select(['broadcast_details.target_id'])->where(['stream_id' => $stream_id])->toArray();

    $targets_in_arr = array();
    foreach ($targets as $t) {
      array_push($targets_in_arr, $t->target_id);
    }
    if(!empty($targets_in_arr)){
      $query = $CommingUsersModel->find();
      $liveBroadData = $query->select(['followersSum' => $query->func()->sum('followers')])->where(['id IN'=> $targets_in_arr])->first(); 
      if(!empty($liveBroadData)){
        return $liveBroadData->followersSum;
      }else{
        return 0;  
      }
    }else{
      return 0;
    }
  }

  function number_shorten($number, $precision = 0, $divisors = null) {
    if($number == 0){
      return 0;
    }
    // Setup default $divisors if not provided
    if (!isset($divisors)) {
        $divisors = array(
            pow(1000, 0) => '', // 1000^0 == 1
            pow(1000, 1) => 'K', // Thousand
            pow(1000, 2) => 'M', // Million
            pow(1000, 3) => 'B', // Billion
            pow(1000, 4) => 'T', // Trillion
            pow(1000, 5) => 'Qa', // Quadrillion
            pow(1000, 6) => 'Qi', // Quintillion
        );    
    }

    // Loop through each $divisor and find the
    // lowest amount that matches
    foreach ($divisors as $divisor => $shorthand) {
        if (abs($number) < ($divisor * 1000)) {
            // We found a match!
            break;
        }
    }
    $numlength = strlen((string)$number);
    if($numlength <= 3){
      $precision = 0;
    }


    // We found our match, or there were no matches.
    // Either way, use the last defined value for $divisor.
    return ($numlength>3)?$this->sigdig($number / $divisor,  '.', $precision) . $shorthand:$number;
  }

  function sigdig($num, $sep='.', $sig=2, $ret=FALSE)
  {
  	$cut = substr($num, 0, ( (strpos($num, $sep)+1)+$sig ));

    if($ret){ return $cut; }
    else{ echo $cut; }
  }

  /*
    Method : getArchiveTableData()
    Title : Used to get archive table data
    author : Sandhya Ghatoda
    Created Date : 06-12-2019
  */
  public function getArchiveTableData(){
      
      $archiveModel = TableRegistry::get("tbl_archive");
      $query = $archiveModel->find(); 
      $query
      ->select([
        'vlike' => $query->func()->sum('vlike'),
        'view' => $query->func()->sum('view'),
        'comment' => $query->func()->sum('comment'),
        'love' => $query->func()->sum('love'),
        'wow' => $query->func()->sum('wow'),
        'angry' => $query->func()->sum('angry'),
        'sad' => $query->func()->sum('sad'),
        'haha' => $query->func()->sum('haha'),
        'share' => $query->func()->sum('share'),
        'reached' => $query->func()->sum('reached')
        ]);

      $details = $query->first();

      return $details; 
  }

} ?>