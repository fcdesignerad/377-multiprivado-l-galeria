<?php 
	$dynamicvideopath = S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO;
?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">

<style>

	html{
		overflow: inherit;
	}

	/** Footer */
	#footer_content{
		background-color: #282828 !important;
		padding: 25px 0;
	}

	.footer{
		background-color: #282828 !important;
	}

	/** Video Gallery */

	#gallery_name{
		color: #000000;
		border-bottom: 1px solid;
		display: inline-block;
		padding: 10px 20px 10px 0;
	}

	#video_gallery #video_gallery_sortable{
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		justify-content: flex-start;
		list-style-type: none;
		width: 100%;
	}

	#video_gallery #video_gallery_sortable li{
		flex: 1;
		flex-basis: 17%;
		margin: 1%;
		flex-grow: 0;
		position: relative;
		cursor: grab;
		box-shadow: 3px 3px 8px 0 #00000024;
   		padding: 5px;
		border: none;
    	background: white;
	}

	#video_gallery .gallery-item{
		width: 100%;
	}

	#video_gallery #add_item{
		width: 100%;
		height: 130px;
		background-color: #58A4A0;
		position: relative;
		transition: 0.5s ease all;
	}

	#video_gallery #add_item:hover{
		background-color: #fccd56;
		cursor: pointer;
	}

	#video_gallery #add_item i{
		font-size: 3.2em;
		position: absolute;
		top: 40%;
		left: 50%;
		transform: translate(-50%);
		color: #ffffff;
	}

	#video_gallery .gallery-item video{
		width: 100%;
		height: 140px;
	}

	#video_gallery .item-information{
		display: grid;
		position: relative;
	}

	#video_gallery .item-information .item-metadata{
		display: none;
	}

	#video_gallery .item-information .item-date{
		display: inherit;
	}

	#video_gallery .item-information.open .item-metadata{
		display: inherit;
		padding: 10px 10px 30px 10px;
	}

	#video_gallery .item-title{
		max-width: 90%;
		margin: 0;
		margin-bottom: 10px;
		width: 100%;
		max-height: 3.5em;	
		color: #000000;
		font-weight: bold;
		font-size: 0.9em;
		text-align: initial;
	}

	#video_gallery .item-metadata{
		text-align: initial;
		color: #656565;
		font-size: 0.85em;
		line-height: 2;
	}

	#video_gallery .item-description-wrapper{
		/* margin: 10px 0; */
		text-align: initial;
	}

	#video_gallery .item-description{
		line-height: 1.6;
	}

	#video_gallery .description-see-more, #video_gallery .description-see-less{
		text-align: initial;
		text-decoration: underline;
		color: #58A4A0;
	}

	.sm-divider{
		display: none;
		margin-top: 20px;
	}

	.grabed{
		cursor: grabbing;
	}

	#video_editor{
		position: fixed;
		width: 40%;
		top: 0;
		bottom: 0;
		right: -40%;
		transition: ease 0.5s all;
		background: #ffffff;
		z-index: 1;
		box-shadow: -1px 0 20px 0 #0000008a;
		padding-top: 35px;
	}

	#video_editor.open{
		right: 0;
	}

	#video_editor #video_form {
		width: 100%;
    	padding: 30px;
	}

	#video_editor #video_form video{
		width: 100%;
		max-height: 450px;
	}

	#main_video_placeholder {
		width: 100%;
		border: 2px dashed #5daaa7;
		height: 280px;
		border-radius: 5px !important;
		cursor: pointer;
	}

	#main_video_placeholder i {
		position: absolute;
		top: 50%;
		right: 50%;
		transform: translate(50%, -50%);
		color: #5daaa7;
		font-size: 4em;
	}

	#main_video_placeholder p {
		position: absolute;
		top: 60%;
		right: 50%;
		transform: translate(50%, -40%);
		color: #5daaa7;
		font-size: 1.6em;
		width: 100%;
		text-align: center;
	}
	
	.form-group{
		position: relative;
	}

	/** BUTTONS STYLE */

	.btn-multiplier{
		color: black;
		border: 1px #00000036 solid;
		border-radius: 5px !important;
		padding: 5px 20px;
		transition: 0.3s ease background;
		cursor: pointer;
		margin: 5px 10px;
	}

	.btn-multiplier:hover{
		background: #fcc946 !important;
		color: white;
	}

	.btn-green{
		color: white;
		background: #5daaa7 !important;
	}

	/** Truncate Text With Ellipsis  */

	.truncate-text{
		overflow: hidden;
		display: -webkit-box;
		-webkit-line-clamp: 3;
		-webkit-box-orient: vertical;
	}

	/** Loader */

	#loader {
		position: absolute;
		left: 50%;
		top: 50%;
		z-index: 1;
		margin: -75px 0 0 -75px;
		border: 5px solid #f3f3f3;
		border-radius: 50% !important;
		border-top: 5px solid #58A4A0;
		width: 100px;
		height: 100px;
		-webkit-animation: spin 2s linear infinite;
		animation: spin 2s linear infinite;
	}

	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}

	/* Add animation to "page content" */
	.animate-bottom {
		position: relative;
		-webkit-animation-name: animatebottom;
		-webkit-animation-duration: 1s;
		animation-name: animatebottom;
		animation-duration: 1s
	}

	@-webkit-keyframes animatebottom {
		from { bottom:-100px; opacity:0 } 
		to { bottom:0px; opacity:1 }
	}

	@keyframes animatebottom { 
		from{ bottom:-100px; opacity:0 } 
		to{ bottom:0; opacity:1 }
	}

	@media(max-width: 1000px){

		#video_gallery .gallery-item {
			flex-basis: 45%;
		}			

	}

	/** Icons */

	.glyphicon-time{
		display: none !important;
	}

	.delete-item-icon, #change_video {
		position: absolute;
		right: 5px;
		top: 5px;
		color: white;
		cursor: pointer;
		text-shadow: 0px 0px 5px #000;
		z-index: 1;
	}

	.edit-item-icon{
		position: absolute;
		right: 0;
    	bottom: 15px;
		color: white;
		cursor: pointer;
		text-shadow: 0px 0px 5px #000;
		z-index: 1;	
	}

	.edit-item-icon:hover, .delete-item-icon:hover, #change_video:hover {
		color: #fcc946;
	}

	.img-zoom {
		transition: 0.5s transform ease;
	}

	.img-zoom:hover {
		transform: scale(1.1);
	}

	/** Messages  */

	.text-box{
		min-width: 220px;
		background: white;
		position: fixed;
		top: 150px;
		right: 50%;
		padding: 10px;
		border-radius: 5px !important;
		z-index: 4;
		transform: translate(50%, 50%);
		text-align: center;
		border: 1px solid #00000059;
		width: 60%;
	}

	.text-box p{
		padding: 0 10px;
	}

	.text-box i{
		font-size: 2em;
    	padding-top: 10px;
	}

	.text-box-button-wraper{
		text-align: center;
	}

	/**   */

	@media(max-width: 1050px){

		#video_gallery #video_gallery_sortable li {
			flex-basis: 22%;
		}			

	}

	@media(max-width: 770px){

		#video_gallery #video_gallery_sortable li {
			flex-basis: 30%;
		}		

		#video_editor {
			width: 55%;
			right: -55%;
		}		

	}

	@media(max-width: 500px){

		#video_gallery #video_gallery_sortable li {
			flex-basis: 45%;
		}		

		#video_editor {
			width: 65%;
			right: -65%;
		}		

	}

	@media(max-width: 380px){

		#video_gallery #video_gallery_sortable{
			padding-left: 20px;
		}

		#video_gallery #video_gallery_sortable li {
			flex-basis: 90%;
		}		

		#video_editor {
			padding-top: 100px;
			width: 95%;
			right: -95%;
		}		

		#video_gallery .gallery-item video {
			height: 140px;
		}

	}

</style>

<h3 id = "gallery_name" > <?= __(gallery) ?> </h3>

<h6 id = "hours_watched"> <?= __(totalHoursWatched) ?> : <?= $total_hours_watched->total_hours_watched ?> </h6>

<div class = "row" id = "video_gallery">

	<ul id = "video_gallery_sortable">

		<li class = "ui-state-default" id = "add_item_wrapper">

			<div class = "gallery-item">

				<div id = "add_item">
					<i class = "fas fa-plus-circle" id = "add_item_icon"></i>
				</div>

				<div class = "item-information">
					<h4 class = "item-title truncate-text"> <?= __(addNewItem) ?> </h4>
					<span class = "item-author item-metadata"> </span>
					<div class = "item-description-wrapper">
						<span class = "item-description item-metadata truncate-text"> </span>
						<a class = "description-see-more item-metadata" type = "button" href = "#"></a>
					</div>
					<span class = "item-date item-metadata"> </span>
					<span class = "item-watched-hours item-metadata"> </span>
					<hr class = "sm-divider">
				</div>

			</div>	

		</li>

	</ul>

</div>

<div id = "video_editor">

	<form class = "form-horizontal form-label-left" id = "video_form" action = "" enctype="multipart/form-data">

		<input class = "hide" id = "video_editor_id">

		<div class = "item form-group">
			
			<input class = "hide" id = "video_editor_new_source_64" type = "file" accept = "video/*">

			<i class="fas fa-edit img-zoom tooltip-tippy" data-tippy-content = "<?= __(addVideoTooltip) ?>" id = "change_video"></i>
			
			<video id = "video_editor_holder" controls>
				<source src = "">
			</video>
		
			<div id = "main_video_placeholder">
				<i class="far fa-play-circle"></i>
				<p> <?= __(addVideoTooltip) ?> </p>
			</div>

		</div>

		<div class = "item form-group" >
			<input class = "form-control" id = "video_editor_title" placeholder = <?= __(title) ?> >
		</div>

		<div class = "item form-group">

			<?= $this->Form->input('video_date', ['label' => false,
					'required' => true,
					'placeholder' => __(date),
					'type' => 'text',
					'class'=>'form-control',
					'id' => 'video_editor_date'
				]);
			?>

		</div>

		<div class = "item form-group">
			<textarea class = "form-control" rows = 6 id = "video_editor_description" placeholder = <?= __(description) ?> ></textarea>
		</div>

		<div class = "item form-group">
			<button class = "btn btn-multiplier btn-green" id = "video_editor_confirm" type = "button"> <?= __(confirmEdition) ?> </button>
			<button class = "btn btn-multiplier" id = "video_editor_cancel" type = "button"> <?= __(Cancel) ?> </button>
		</div>

	</form>

</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="https://unpkg.com/popper.js@1"></script>
<script src="https://unpkg.com/tippy.js@5"></script>



<script>

	/** Gallery JS */

	var gallery_itens = <?= json_encode($application_videos) ?>;

	var author = " Prefeitura de Petrópolis ";

	$(document).ready(function(){

		addNewVideosToGallery();

		$("#video_gallery_sortable").sortable({
			placeholder: "ui-state-highlight",
			start: function(event, ui){
				$(ui.item.get(0)).addClass("grabed");
				console.log('start sort');
			},
			update: function(event, ui){
				$(ui.item.get(0)).removeClass("grabed");
				console.log('end sort');
			},
			cancel: "#add_item_wrapper",
		}).disableSelection();

		$("#add_item, #add_item_icon").click(function(){
			openVideoEditor('', true);
		});
		
		$("#change_video, #main_video_placeholder").click(function(){
			$("#video_editor_new_source_64").click();
		});

		$("#video_editor_new_source_64").change(function(){

			var file_input = this;

			setTimeout(function(){
				$("#video_editor_holder").children("source").attr("src", URL.createObjectURL(file_input.files[0]) );
				$("#video_editor_holder").get(0).load();

				$("#video_editor_holder, #change_video").removeClass("hide");
				$("#main_video_placeholder").addClass("hide");

			}, 200);

		});

		$("#video_editor_confirm").click(function(){

			if( $("#video_editor_title").val().trim().length === 0 || $("#video_editor_date").val().trim().length === 0 ){
				showMessage('validation');
				return false;
			}

			var form_data = new FormData();
			form_data.append("file", document.getElementById('video_editor_new_source_64').files[0]);
			form_data.append('title', $("#video_editor_title").val().trim() );
			form_data.append('date', $("#video_editor_date").val().trim() );
			form_data.append('description', $("#video_editor_description").val().trim() );
			form_data.append('video_editor_id', $("#video_editor_id").val().trim() );
			form_data.append('application_id', <?= $application_id ?>);

			var url = window.location.origin + "/app/webregistration/addVideoGallery";
			
			$.ajax({	
				url:url,
				method:"POST",
				data: form_data,
				contentType: false,
				cache: false,
				dataType: "text",
				processData: false,
				beforeSend:function(){
					$("#video_editor_confirm, #video_editor_cancel").attr("disabled", true)
					addLoader();
				},
				success:function(dt) {
					data = JSON.parse(dt);
					
					if($("#video_editor_id").val().length > 0){
						var $item = $(`.gallery-item[data-id='${$("#video_editor_id").val()}']`);
						changeItem($item);
					}
					else{

						var item = {
							id: data.id,
							video_name: data.video_name,
							title: $("#video_editor_title").val(),
							author: author,
							description: $("#video_editor_description").val(), 
							video_date: $("#video_editor_date").val(),
						}

						addVideo(item, 0);
					}

				},
				complete: function() {
					$("#video_editor_confirm, #video_editor_cancel").attr("disabled", false);
					removeLoader();
					closeVideoEditor();
				}
			});

		});

		$("#video_editor_cancel").click(function(){
			closeVideoEditor();
		});

		tippy(".tooltip-tippy");

		$('#video_editor_date').daterangepicker({
			singleDatePicker: true,
			showDropdowns: false,
			minYear: 1901,
			timePicker: true,
			timePicker24Hour: true,
			autoUpdateInput: false,
		}).on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('YYYY-MM-DD HH:mm:ss'));
		});

	});

	$(document).on('click', '.delete-item-icon', function(e){
		showMessage("deleteVideo", $(this) );
	});

	$(document).on('click', '.edit-item-icon', function(e){
		openVideoEditor($(this).parent().parent());
	});

	$(document).click(function(event){

		if( $(event.target).closest("#video_editor").length === 0 && !$(event.target).hasClass("edit-item-icon") && !$(event.target).is("#add_item_icon") &&
			!$(event.target).is("#add_item") && !$(event.target).is(".daterangepicker") && $(event.target).closest(".daterangepicker").length === 0 && !$(event.target).is(".glyphicon-chevron-right, .glyphicon-chevron-left, .next, .prev") 
			&& $(event.target).closest(".table-condensed").length === 0 ){
			closeVideoEditor();
		}

	});

	/**
	 *
	 */
	function openVideoEditor($item, new_video = false){

		clearVideoEditor();

		$("#video_editor").addClass("open");

		if(new_video){
			clearVideoEditor();
			$("#video_editor_holder, #change_video").addClass("hide");
			$("#main_video_placeholder").removeClass("hide");
		}
		else{

			$("#video_editor_holder, #change_video").removeClass("hide");
			$("#main_video_placeholder").addClass("hide");

			$("#video_editor_holder").children("source").attr("src", $item.find("source").attr("src") );
			$("#video_editor_holder").get(0).load();

			$("#video_editor_id").val( $item.attr("data-id") );
			$("#video_editor_title").val( $item.find(".item-title").text().trim() );
			$("#video_editor_date").val( $item.find(".item-date").text().trim() );
			$("#video_editor_description").val( $item.find(".item-description").text().trim() );
		}

	}

	/**
	 *
	 */
	function closeVideoEditor(){

		$("#video_editor").removeClass("open");
		clearVideoEditor();
	}

	/**
	 *
	 */
	function clearVideoEditor(){

		$("#video_editor_holder").children("source").attr("src", "" );
		$("#video_editor_holder").get(0).load();
		$("#video_editor_new_source_64").val();

		$("#video_editor_id").val( "" );
		$("#video_editor_title").val( "" );
		$("#video_editor_date").val( "" );
		$("#video_editor_description").text( "" ).val("");

	}

	/**
	 *
	 */
	function changeItem($item){

		$item.find("source").attr("src", $("#video_editor_holder").children("source").attr("src") );
		$item.find("video").get(0).load();

		$item.find(".item-title").text( $("#video_editor_title").val() );
		$item.find(".item-date").text( $("#video_editor_date").val() );
		$item.find(".item-description").text( $("#video_editor_description").text() );
	}

	/**
		* Add a new video to the end of the gallery
		*/
	function addVideo( item = {}, append = true ){

		item = (!jQuery.isEmptyObject(item)) ? item : gallery_itens.shift();

		if(append){

			$("#video_gallery_sortable").append(`
				<li class = "ui-state-default">
					<div class = "gallery-item" data-id = ${item.id}>

						<i class="fas fa-trash-alt img-zoom delete-item-icon tooltip-tippy" data-tippy-content = "<?= __(remove) ?>"></i>

						<video controls>
							<source src = "<?= $dynamicvideopath ?>${item.video_name}">
						</video>

						<div class = "item-information">
							<h4 class = "item-title truncate-text" title = "${item.title}"> ${item.title} </h4>
							<i class="fas fa-edit edit-item-icon tooltip-tippy" data-tippy-content = "<?= __(editItemGallery) ?>" ></i>
							<span class = "item-author item-metadata"> ${item.author} </span>
							<div class = "item-description-wrapper">
								<span class = "item-description item-metadata truncate-text"> ${item.description} </span>
							</div>
							<span class = "item-date item-metadata"> ${item.video_date_only} </span>
							<hr class = "sm-divider">
						</div>

					</div>
				</li>
			`);

		}
		else{

			$("#add_item_wrapper").after(`
				<li class = "ui-state-default">
					<div class = "gallery-item" data-id = ${item.id}>

						<i class="fas fa-trash-alt img-zoom delete-item-icon tooltip-tippy" data-tippy-content = "<?= __(remove) ?>"></i>

						<video controls>
							<source src = "<?= $dynamicvideopath ?>${item.video_name}">
						</video>

						<div class = "item-information">
							<h4 class = "item-title truncate-text" title = "${item.title}"> ${item.title} </h4>
							<i class="fas fa-edit img-zoom edit-item-icon tooltip-tippy" data-tippy-content = "<?= __(editItemGallery) ?>"></i>
							<span class = "item-author item-metadata"> ${item.author} </span>
							<div class = "item-description-wrapper">
								<span class = "item-description item-metadata truncate-text"> ${item.description} </span>
							</div>
							<span class = "item-date item-metadata"> ${item.video_date_only} </span>
							<hr class = "sm-divider">
						</div>

					</div>
				</li>
			`);

		}

	}

	/**
	 * Add news videos to the gallery when you reach the end of the scroll
	 */
	function addNewVideosToGallery(){

		setTimeout(function(){

			$.each(gallery_itens, function(index, value){ 
				addVideo();
			});

			$(".item-title").tooltip();
			tippy(".tooltip-tippy");
			removeLoader();

		}, 1500);
		
	}

	/**
	 * Checks if the element has been scrolled to the end
	 */
	function scrolledToBottom(element){
		return (element.offsetHeight + element.scrollTop >= element.scrollHeight - 20) ? true : false;
	}

	/**
	 * Add the loader 
	 */
	function addLoader(target){
		$(".page-content").append(`<div id="loader"></div>`);
	}

	/**
	 * Remove the loader
	 */
	function removeLoader(){
		$("#loader").remove();
	}

	/**
	 *
	 */
	function deleteVideo($trashCanElement){

		var url = `${window.location.origin}/app/webregistration/deleteVideoGallery/${ $trashCanElement.parent().attr("data-id") }`;

		var $wrapper = $trashCanElement.parent().parent();

		$.ajax({	
			url:url,
			method:"POST",
			contentType: false,
			cache: false,
			dataType: "text",
			processData: false,
			beforeSend:function(){
				addLoader();
			},
			success:function(dt) {
				data = JSON.parse(dt);

				if(data){
					$wrapper.remove();
				}
				else{
					showMessage("cantDeleteVideo");
				}

			},
			complete: function() {
				removeLoader();
			}
		});
	}

	/**
	 *
	 */
	function showMessage(type, target = ""){

		$target = target;

		switch(type){
			case "deleteVideo":

				$(".page-content").append(`
					<div class = "text-box" style = 'width: auto;'> 
						<i class="fas fa-info img-zoom" id = "info_message"></i>
						<h3> <?= __(attention) ?> </h3>
						<p> <?= __(deletingFile) ?> </p> 
						<div class = "text-box-button-wraper">
							<a class = "btn btn-multiplier btn-green" href = "#" onclick = "deleteVideo( $target ); $('.text-box').remove();"> <?= __(delete) ?> </a>
							<a class = "btn btn-multiplier" onclick = "$('.text-box').remove();" > <?= __(go_back) ?> </a>
						</div>
					</div>`
				);

				break;
			case "validation":

				$(".page-content").append(`
					<div class = "text-box" style = 'width: auto;'> 
						<i class="fas fa-info img-zoom" id = "info_message"></i>
						<h3> <?= __(attention) ?> </h3>
						<p> <?= __(emptyRequiredFields) ?> </p> 
						<div class = "text-box-button-wraper">
							<a class = "btn btn-multiplier" onclick = "$('.text-box').remove();" > <?= __(ok) ?> </a>
						</div>
					</div>`
				);

				break;
			case "cantDeleteVideo":

				$(".container").append(`
					<div class = "text-box" style = 'width: auto;'> 
						<i class="fas fa-info img-zoom" id = "info_message"></i>
						<h3> <?= __(attention) ?> </h3>
						<p> <?= __(dontDeleteFile) ?> </p> 
						<div class = "text-box-button-wraper">
							<a class = "btn btn-multiplier btn-green" href = "#" onclick = "$('.text-box').remove();"> <?= __(ok) ?> </a>
						</div>
					</div>`
				);

				break;
			default:
				console.log('error message');
		}

	}


</script>
