<?php
namespace App\Controller;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\Network\Email\Email;
use Cake\Event\Event;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Aws\S3\S3Client;
use Cake\I18n\Time;
//static Cake\Core\Configure::write($key, $value);
/**
* Static content controller
*
* This controller will render views from Template/Pages/
*
* @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
*/
class WebregistrationController extends AppController
{
    public $helpers = ['Form'];
    public $paginate = [
         'limit' => 25,
         'order' => [
         'Userss.email' => 'asc'
         ]];
   //$this->loadComponent('Resize');
   //Function for check admin session
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }
    public function initialize()
    {
		parent::initialize();
        $this->loadComponent('Paginator');
	}

	//function to load web registration user listing
	function formlisting()
	{
        $this->viewBuilder()->layout('admin_inner');
        $userId = $this->Auth->user('id');
        $userRole = $this->Auth->user('role');
        $userParent = $this->Auth->user('parent_id');
        $userSegment = $this->Auth->user('segment_id');
        $SegmentModel = TableRegistry::get('Segments');
        $SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
        $userSegmentId=0;
        if(!empty($SegmentData)){
            $userSegmentId = $SegmentData->segment_admin;
        }
        $usersModel = TableRegistry::get("Users");
        $usersList  = $usersModel->find('list', [
            'keyField' => 'id',
            'valueField' => 'id'
        ])->where(['segment_id' =>$userSegment,'role !=' =>5])->toArray();
        $usersList[$userId] = $this->Auth->user('id');
        $applicationModel = TableRegistry::get("applications");
        if($userRole == "5"){
            if($userId==$userSegmentId){
                $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['Users.id IN'=> $usersList,'id_web_registration_enabled'=>true])->group('group_id')->toArray();
            }else{
                $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['Users.id' =>$userId,'id_web_registration_enabled'=>true])->group('group_id')->toArray();
            }
        }else{
            $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['id_web_registration_enabled'=>true])->group('group_id')->toArray();
            //$applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['id_web_registration_enabled'=>true])->toArray();
        }
        $segmentModel = TableRegistry::get("segments");
        $segments  = $segmentModel->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])->where(['status' => 1])->toArray();
       // print_r($segments);die;
        $this->set(compact('applications','segments'));
        $this->set('_serialize', ['applications']);
        $this->set('page','webregistration');
	}
	function userlisting()
	{
        $this->viewBuilder()->layout('admin_inner');
        $userId = $this->Auth->user('id');
        $userRole = $this->Auth->user('role');
        $userParent = $this->Auth->user('parent_id');
        $userSegment = $this->Auth->user('segment_id');
        $SegmentModel = TableRegistry::get('Segments');
        $SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
        $userSegmentId=0;
        if(!empty($SegmentData)){
            $userSegmentId = $SegmentData->segment_admin;
        }
        $usersModel = TableRegistry::get("Users");
        $usersList  = $usersModel->find('list', [
            'keyField' => 'id',
            'valueField' => 'id'
        ])->where(['segment_id' =>$userSegment,'role !=' =>5])->toArray();
        $usersList[$userId] = $this->Auth->user('id');
        $applicationModel = TableRegistry::get("applications");
        if($userRole == "5"){
            if($userId==$userSegmentId){
                $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['Users.id IN'=> $usersList,'id_web_registration_enabled'=>true])->group('group_id')->toArray();
            }else{
                $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['Users.id' =>$userId,'id_web_registration_enabled'=>true])->group('group_id')->toArray();
            }
        }else{
            //$applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['id_web_registration_enabled'=>true])->group('group_id')->toArray();
            $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['id_web_registration_enabled'=>true])->toArray();
        }
        $segmentModel = TableRegistry::get("segments");
        $segments  = $segmentModel->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])->where(['status' => 1])->toArray();
        // print_r($segments);die;
        $this->set(compact('applications','segments'));
        $this->set('_serialize', ['applications']);
        $this->set('page','webregistration');
	}

	//function to load view for form
    public function editForm($applicationId=null)
    {

        $this->viewBuilder()->layout('admin_inner');
        $WebRegistrationModel = TableRegistry::get("tbl_webregistration_form");
        $applicationModel = TableRegistry::get("applications");
        $this->set('page','webregistration');
        $this->set('editor',true);
        $record_found=false;
        if(!empty($applicationId)){
            $applicationData=$applicationModel->find('all')->where(['id'=>$applicationId,'id_web_registration_enabled'=>true])->first();
            if(!empty($applicationData)){
                $record_found=true;
            }
            $WebRegistrationData=$WebRegistrationModel->find('all')->where(['application_id'=>$applicationId,'language'=>$this->siteLanguage()])->first();
            $this->isFormOwner($applicationData);
            $this->set('formInfo',$WebRegistrationData);
            $this->set('applicationId',$applicationId);
        }
        if($record_found==false){
            $this->redirect(['controller' => 'Webregistration', 'action' => 'formlisting']);
        }
        if(isset($this->request->data) && !empty($this->request->data))
        {

           // print_r($applicationData);die;
            $formId =  $this->request->data['form_id'];
            $WebRegistrationData = $WebRegistrationModel->newEntity();
            if (!file_exists(WWW_ROOT . '/img/uploads/webregimg/logo/')) {
                //$oldMask = umask(0);
                mkdir(WWW_ROOT . '/img/uploads/webregimg/logo/', 0777, true);
                chmod(WWW_ROOT . '/img/uploads/webregimg/logo/', 0777);
                //umask($oldMask);
            }
            if (is_dir(WWW_ROOT . '/img/uploads/webregimg/logo/'))
            {

                $logo = $this->request->data['logo'];
                if( !empty( $logo['name'] ) )
                {
                    $form_img1_info 	=  pathinfo( $logo['name'] );
                    $form_img1_ext 		= $form_img1_info['extension'];
                    $form_img1_new_name = time() . rand(1,999) . '.' . $form_img1_ext;
                    if( $form_img1_ext == 'jpg' || $form_img1_ext == 'jpeg' || $form_img1_ext == 'png' || $form_img1_ext == 'JPG' || $form_img1_ext == 'JPEG'  || $form_img1_ext == 'PNG')
                    {
                        if( !$k = move_uploaded_file($logo['tmp_name'], WWW_ROOT . 'img/uploads/webregimg/logo/' . $form_img1_new_name ) )
                        {
                            $this->Flash->error(__(image_updaload_error));
                            $this->redirect(['controller' => 'Webregistration', 'action' => 'edit-form/'.$applicationId]);
                            echo 'logo having error';
                            //die(image_updaload_error);
                        }else {
                            $WebRegistrationData->logo = @$form_img1_new_name;
                        }
                    }
                    else
                    {
                        $this->Flash->error(__(image_updaload_error));
                        $this->redirect(['controller' => 'Webregistration', 'action' => 'edit-form/'.$applicationId]);
                        echo 'Logo file type not allowed';
                        //die(image_updaload_error);
                    }
                }else{
					$img_status1 = $this->request->data['img_status1'];
					if(img_status1==1){
						$WebRegistrationData->logo ='';
					}
                }
            }

            if (!file_exists(WWW_ROOT . '/img/uploads/webregimg/banner/')) {
                //$oldMask = umask(0);
                mkdir(WWW_ROOT . '/img/uploads/webregimg/banner/', 0777, true);
                chmod(WWW_ROOT . '/img/uploads/webregimg/banner/', 0777);
                //umask($oldMask);
            }
            if (is_dir(WWW_ROOT . '/img/uploads/webregimg/banner/')) {
                $bannerimage = $this->request->data['bannerimage'];
                if (!empty($bannerimage['name'])) {
                    $form_img2_info = pathinfo($bannerimage['name']);
                    $form_img2_ext = $form_img2_info['extension'];
                    $form_img2_new_name = time() . rand(1, 999) . '.' . $form_img2_ext;
                    if ($form_img2_ext == 'jpg' || $form_img2_ext == 'jpeg' || $form_img2_ext == 'png' || $form_img2_ext == 'JPG' || $form_img2_ext == 'JPEG' || $form_img2_ext == 'PNG') {
                        if (!move_uploaded_file($bannerimage['tmp_name'], WWW_ROOT . 'img/uploads/webregimg/banner/' . $form_img2_new_name)) {
                            $this->Flash->error(__('Unable to upload Image 2 '));
                            $this->redirect(['controller' => 'Webregistration', 'action' => 'edit-form/'.$applicationId]);
                            echo 'Cover file having error';
                           // die('Error');
                        }
                        $WebRegistrationData->cover_image = @$form_img2_new_name;
                    } else {
                        $this->Flash->error(__(image_updaload_error));
                        $this->redirect(['controller' => 'Webregistration', 'action' => 'edit-form/'.$applicationId]);
                        echo 'Cover file type not allowed';
                        //die(image_updaload_error);
                    }
                }else{
					$img_status2 = $this->request->data['img_status2'];
					if(img_status2==1){
						$WebRegistrationData->cover_image ='';
					}
                }
            }

			

            $WebRegistrationData->description =@$this->request->data['form_description'];
            $WebRegistrationData->title =@$this->request->data['form_title'];
            $WebRegistrationData->application_id =$applicationData->id;
            $WebRegistrationData->manager_id =$applicationData->manager_id;
            $WebRegistrationData->theme_color =@$this->request->data['theme_color'];
            $WebRegistrationData->text_color =@$this->request->data['text_color'];
			
			
			$WebRegistrationData->facebook_url =@$this->request->data['facebook_url'];
			$WebRegistrationData->twitter_url =@$this->request->data['twitter_url'];
			$WebRegistrationData->website_url =@$this->request->data['website_url'];
			$WebRegistrationData->skype_url  =@$this->request->data['skype_url'];
			$WebRegistrationData->linkedIn_url  =@$this->request->data['linkedIn_url'];
			$WebRegistrationData->youtube_url  =@$this->request->data['youtube_url'];
			$WebRegistrationData->company_email =@$this->request->data['company_email'];
			$WebRegistrationData->company_name =@$this->request->data['company_name'];
			$WebRegistrationData->footer_title =@$this->request->data['footer_title'];
			$WebRegistrationData->footer_sub_title =@$this->request->data['footer_sub_title'];
			
			$WebRegistrationData->language=$this->siteLanguage();

            if($formId){
               // print_r($WebRegistrationData);die;
                $data=['description'=>$WebRegistrationData->description,'title'=>$WebRegistrationData->title,'application_id'=>$WebRegistrationData->application_id,'manager_id'=>$WebRegistrationData->manager_id,'theme_color'=>$WebRegistrationData->theme_color,'text_color'=>$WebRegistrationData->text_color,'facebook_url'=>$WebRegistrationData->facebook_url,'twitter_url'=>$WebRegistrationData->twitter_url,'website_url'=>$WebRegistrationData->website_url,'skype_url'=>$WebRegistrationData->skype_url,'linkedIn_url'=>$WebRegistrationData->linkedIn_url,'youtube_url'=>$WebRegistrationData->youtube_url,'company_email'=>$WebRegistrationData->company_email,'footer_sub_title'=>$WebRegistrationData->footer_sub_title,'company_name'=>$WebRegistrationData->company_name,'footer_title'=>$WebRegistrationData->footer_title];
                if($WebRegistrationData->cover_image){
                    $data['cover_image']=$WebRegistrationData->cover_image;
                }else{
					$img_status2 = $this->request->data['img_status2'];
					if($img_status2==1){
						$data['cover_image']='';
					}
                }
                if($WebRegistrationData->logo){
                    $data['logo']=$WebRegistrationData->logo;
                }else{
					$img_status1 = $this->request->data['img_status1'];
					if($img_status1==1){
						$data['logo']='';
					}
                }
                $WebRegistrationModel->updateAll($data,['id'=>$formId,'language'=>$WebRegistrationData->language]);
				$WebRegistrationModel->updateAll(['facebook_url'=>$WebRegistrationData->facebook_url,'twitter_url'=>$WebRegistrationData->twitter_url,'website_url'=>$WebRegistrationData->website_url,'skype_url'=>$WebRegistrationData->skype_url,'linkedIn_url'=>$WebRegistrationData->linkedIn_url,'youtube_url'=>$WebRegistrationData->youtube_url],['application_id'=>$applicationData->id,'language'=>'English']);
				$WebRegistrationModel->updateAll(['facebook_url'=>$WebRegistrationData->facebook_url,'twitter_url'=>$WebRegistrationData->twitter_url,'website_url'=>$WebRegistrationData->website_url,'skype_url'=>$WebRegistrationData->skype_url,'linkedIn_url'=>$WebRegistrationData->linkedIn_url,'youtube_url'=>$WebRegistrationData->youtube_url],['application_id'=>$applicationData->id,'language'=>'Portuguese']);
				$WebRegistrationModel->updateAll(['facebook_url'=>$WebRegistrationData->facebook_url,'twitter_url'=>$WebRegistrationData->twitter_url,'website_url'=>$WebRegistrationData->website_url,'skype_url'=>$WebRegistrationData->skype_url,'linkedIn_url'=>$WebRegistrationData->linkedIn_url,'youtube_url'=>$WebRegistrationData->youtube_url],['application_id'=>$applicationData->id,'language'=>'Spanish']);
				$response=true;
            }else{
               $response= $WebRegistrationModel->save($WebRegistrationData);
            }
            if($response){
                $this->Flash->success(__(updaload_success));
                $this->redirect(['controller' => 'Webregistration', 'action' => 'edit-form/'.$applicationId]);
            }
        }
    }

    //function to load web registration user listing
    function viewUsers($application_id)
    {
        $this->viewBuilder()->layout('admin_inner');
        $userId = $this->Auth->user('id');
        $userRole = $this->Auth->user('role');
        $userParent = $this->Auth->user('parent_id');
        $webUserModel = TableRegistry::get("multiprivate_users");
        $webUser  = $webUserModel->find('all')->contain('Users')->where(['application_id'=>$application_id])->order('created_datetime desc')->toArray();
        $this->set(compact('webUser','application_id'));
        $this->set('_serialize', ['webUser']);
        $this->set('page','webregistration');
    }

    //function to import user
    function importUser(){
        //$filePointer = fopen($this->request->data['import']['tmp_name'], "rb");
        // open the file
        $application_id=$this->request->data['application_id'];
        $handle = fopen($this->request->data['import']['tmp_name'], "r");

        // read the 1st row as headings
        $header = fgetcsv($handle);

        // create a message container
        $return = array(
            'messages' => array(),
            'errors' => array(),
        );
        $i=0;
        $applicationModel = TableRegistry::get("applications");
        $applicationData=$applicationModel->find('all')->where(['id'=>$application_id,'id_web_registration_enabled'=>true])->first();
        $webUserModel = TableRegistry::get("multiprivate_users");
        // read each data row in the file
        while (($row = fgetcsv($handle)) !== FALSE) {
            $is_email_exists=$webUserModel->find('all')->where(['application_id'=>$application_id,'userEmail'=>$row[1]])->first();
            if(empty($is_email_exists)) {
                $webUserdata = $webUserModel->newEntity();
                $webUserdata->firstName=$name= trim($row[0]);
                $webUserdata->userEmail =$email=trim($row[1]);
				$password=$row[2];
				if($password==''){
					$password=$this->randomPassword();
				}
                $webUserdata->userPassword = md5($password);
                $webUserdata->application_id = $application_id;
                $webUserdata->manager_id = $applicationData->manager_id;
               // if ($this->user('role') == 1 || $this->user('role') == 5) {
                    $webUserdata->userStatus = true;
                //}
                $response=$webUserModel->save($webUserdata);
                if($response){
                   // if ($this->user('role') == 1 || $this->user('role') == 5) {
                        $data = array( 'email' => $email, 'password' => $password, 'subject' => __(Web_registration_mail_subject), 'name' => $name, 'email_heading' => __(Web_registration_mail_subject), 'form_link' => WEB_REGISTRATION_URL.md5($application_id) );
                        $this->send_email_html($data,'web_registration_user_created');
                   // }
                }
            }
        }
        // close the file
        fclose($handle);
        $this->Flash->success(__(updaload_success));
        $this->redirect(['controller' => 'Webregistration', 'action' => 'view-users/'.$application_id]);
    }

    //function to approve user
    function approval($application_id,$user_id,$status){
        $webUserModel = TableRegistry::get("multiprivate_users");
        $webUserdata=$webUserModel->find('all')->where(['userId'=>$user_id])->first();
        if(!empty($webUserdata)){
            $webUserModel->updateAll(['userStatus'=>$status],['userId'=>$user_id]);

            if($status == 1){
            // Create group
            $imGroupModel = TableRegistry::get("im_group");
            $imGrpData=$imGroupModel->find('all')->where(['application_id'=>$application_id])->first();

            $imGroupMemberModel = TableRegistry::get("im_group_members");

            if(count($imGrpData) == 1){

                $dt=$imGroupMemberModel->find('all')->where(['g_id'=>$imGrpData->g_id, 'u_id'=>$user_id])->first();

                if(empty($dt)){

                    $imGroupMemberdata = $imGroupMemberModel->newEntity();
                    $imGroupMemberdata->g_id= $imGrpData->g_id;
                    $imGroupMemberdata->u_id= $user_id;
                    $imGroupMemberModel->save($imGroupMemberdata);
                }

            }else{

                $imGroupdata = $imGroupModel->newEntity();
                $imGroupdata->createdBy= $user_id;
                $imGroupdata->type= 0;
                $imGroupdata->block= 0;
                $imGroupdata->lastActive = date('Y-m-d H:i:s');
                $imGroupdata->application_id = $application_id;
                $response=$imGroupModel->save($imGroupdata);

                if($response){
                    $imGroupMemberdata = $imGroupMemberModel->newEntity();
                    $imGroupMemberdata->g_id= $response->g_id;
                    $imGroupMemberdata->u_id= $user_id;
                    $imGroupMemberModel->save($imGroupMemberdata);
                }
            }
        }
        }
        $this->redirect(['controller' => 'Webregistration', 'action' => 'view-users/'.$application_id]);
    }
    //function to delete user
    function delete($application_id,$user_id){
        $webUserModel = TableRegistry::get("multiprivate_users");
        $webUserdata=$webUserModel->find('all')->where(['userId'=>$user_id])->first();
        if(!empty($webUserdata)){
            $webUserModel->delete($webUserdata);
        }
        $this->redirect(['controller' => 'Webregistration', 'action' => 'view-users/'.$application_id]);
    }
    function isFormOwner($applicationData)
    {
        if ($this->user('role') != 1 && $this->user('role') != 5)
        {
            if ($applicationData->manager_id!=$this->Auth->user('id'))
            {
                $this->Flash->success(__(error_403));
                return $this->redirect(['controller' => 'Webregistration', 'action' => 'formlisting']);
            }
        }
    }

    public function editUserForm($applicationId=null,$user_id=null)
    {

        $this->viewBuilder()->layout('admin_inner');
        $WebUserModel = TableRegistry::get("multiprivate_users");
        $this->set('page','webregistration');
        $this->set('editor',true);
        $record_found=false;
        if(!empty($applicationId)){
            $webUserData=$WebUserModel->find('all')->where(['userId'=>$user_id])->first();
            $this->set('id_user_active_checked', '');
            if(!empty($webUserData)){
                $record_found=true;
                /*if($webUserData->is_active){
                    $this->set('id_user_active_checked', 'checked');
                }else{
                    $this->set('id_user_active_checked', '');
                }*/
            }

            $this->set('formInfo',$webUserData);
            $this->set('applicationId',$applicationId);
            $this->set('user_id',$user_id);
        }
        if($record_found==false){
            $this->redirect(['controller' => 'Webregistration', 'action' =>'view-users/'.$applicationId]);
        }
        if(isset($this->request->data) && !empty($this->request->data))
        {
			//,'is_active'=>isset($this->request->data['is_active'])?1:0
            if($user_id){
                // print_r($WebRegistrationData);die;
                $data=['firstName'=>trim($this->request->data['name']),'userEmail'=>trim($this->request->data['email']),'userPassword'=>md5($this->request->data['password'])];
                $WebUserModel->updateAll($data,['userId'=>$user_id]);
            }
             $this->Flash->success(__(updaload_success));
             $this->redirect(['controller' => 'Webregistration', 'action' =>'view-users/'.$applicationId]);
        }
    }
	
	public function addUserForm($applicationId=null)
    {

        $this->viewBuilder()->layout('admin_inner');
        $WebUserModel = TableRegistry::get("multiprivate_users");
        $this->set('page','webregistration');
        $this->set('editor',true);
        $record_found=false;
        if(!empty($applicationId)){
            $this->set('applicationId',$applicationId);
        }
		
		$applicationModel = TableRegistry::get("applications");
       	$applicationData=$applicationModel->find('all')->where(['id'=>$applicationId,'id_web_registration_enabled'=>true])->first();
        if(isset($this->request->data) && !empty($this->request->data))
        {
			$is_email_exists=$WebUserModel->find('all')->where(['application_id'=>$applicationId,'userEmail'=>trim($this->request->data['email'])])->first();
            if(empty($is_email_exists)) {
				$webUserdata = $WebUserModel->newEntity();
				$webUserdata->firstName=$name= trim($this->request->data['name']);
				$webUserdata->userEmail =$email=trim($this->request->data['email']);
				$password=trim($this->request->data['password']);
				if($password==''){
					$password=$this->randomPassword();
				}
				$webUserdata->userPassword = md5($password);
				$webUserdata->application_id = $applicationId;
				$webUserdata->manager_id = $applicationData->manager_id;
				   // if ($this->user('role') == 1 || $this->user('role') == 5) {
				$webUserdata->userStatus = true;
					//}
				$response=$WebUserModel->save($webUserdata);
				 if($response){
					 $data = array( 'email' => $email, 'password' => $password, 'subject' => __(Web_registration_mail_subject), 'name' => $name, 'email_heading' => __(Web_registration_mail_subject), 'form_link' => WEB_REGISTRATION_URL.md5($application_id) );
					 $this->send_email_html($data,'web_registration_user_created');
					 $this->Flash->success(__(updaload_success));
					 $this->redirect(['controller' => 'Webregistration', 'action' =>'view-users/'.$applicationId]);
				 }
			}else{
					 $this->Flash->error(__(verifyEmailErr));
					 $this->redirect(['controller' => 'Webregistration', 'action' =>'view-users/'.$applicationId]);
			}
        }
    }

    public function golive($id=null){  
        $this->viewBuilder()->layout('admin_inner');
        if(!empty($id)){   
            $Applications = TableRegistry::get('Applications'); 
            $applicationData = $Applications->find('all')->where(['id' =>$id])->first();
            $FormListingModel = TableRegistry::get("form_listings");    
            $FormSingleData=$FormListingModel->find('all')->where(['owner_id'=>$applicationData['manager_id']])->first();
              $groupsModel = TableRegistry::get("groups");    
            $groupsModelData=$groupsModel->find('all')->where(['owner_id'=>$applicationData['manager_id']])->first();
            $this->set('GroupSingleData',$groupsModelData);
            $this->set('FormSingleData',$FormSingleData);
            $this->set('applicationData',$applicationData);
             $this->set('page','webregistration');
        }
    }


    public function deleteChat($id=null)
    {
        $this->viewBuilder()->layout('');
        $this->set('application_id',$id);

        if(isset($this->request->data)&& !empty($this->request->data))
        {   
            $multUsersModel = TableRegistry::get("multiprivate_users");
            $multUsersData = $multUsersModel->find('all')->select(['userId'])->where(['application_id' =>$this->request->data('application_id')])->toArray(); 
            
            $idArr = array();
            if(count($multUsersData) > 0){
                foreach ($multUsersData as $data) {
                    array_push($idArr,$data->userId);
                    
                }

                $strarr = implode(",",$idArr);
                $conn = ConnectionManager::get('default');
                $query = "DELETE  FROM `im_message` WHERE sender IN(".$strarr.") OR receiver IN(".$strarr.")";
                $conn->execute( $query );
                $this->Flash->success(__(record_deleted));
                $this->redirect(['controller' => 'webregistration', 'action' => 'view-users/'.$this->request->data('application_id')]);  
        }
            
        }
        $this->render('modal/delete-chat');
    }


    function blockusers($application_id,$user_id,$status){
        $webUserModel = TableRegistry::get("multiprivate_users");
        $webUserdata=$webUserModel->find('all')->where(['userId'=>$user_id])->first();
        if(!empty($webUserdata)){
            $webUserModel->updateAll(['blockStatus'=>$status],['userId'=>$user_id]);
            
        }
        $this->redirect(['controller' => 'Webregistration', 'action' => 'view-users/'.$application_id]);
	}
	
	// MOVE THESE METHODS TO THE RELEVANT CONTROLLER!!!!!!!!!!!
	//METHODS FOR THE VIDEO GALLERY

	public function videoGallery($application_id){

		$this->viewBuilder()->setLayout('admin_inner');

		$ApplicationVideoTable = TableRegistry::get('ApplicationVideos');

		$application_videos = $ApplicationVideoTable->find()
			->select([
				"ApplicationVideos.id",
				"ApplicationVideos.video_name",
				"ApplicationVideos.title",
				"ApplicationVideos.description",
				"video_date_only" => "DATE_FORMAT(ApplicationVideos.video_date, '%d/%m/%Y')",
				"video_hours_watched" => "TIME(ApplicationVideos.hours_watched)"
			])
			->where(["ApplicationVideos.application_id" => $application_id])
			->order("ApplicationVideos.video_date DESC")
			->toArray();

		$total_hours_watched = $ApplicationVideoTable->find()
			->select(["total_hours_watched" => "SEC_TO_TIME( SUM( TIME_TO_SEC(ApplicationVideos.hours_watched) ) )"])
			->where(["ApplicationVideos.application_id" => $application_id])
			->group("ApplicationVideos.application_id")
			->first();

		$this->set("application_videos", $application_videos);
		$this->set("application_id", $application_id);
		$this->set("total_hours_watched", $total_hours_watched);
	}

	/**
	 * Add new videos or edit existing ones
	 */
	public function addVideoGallery(){

        if($_FILES["file"]["name"] != ''){

            $filename = str_replace(' ', '',basename($_FILES['file']['name']));
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $filename = time().".".$ext;
            $alphanum = "abcdefghijkmnpqrstuvwxyz23456789";
            $codekey = substr(str_shuffle($alphanum), 0, 8);
            $filename = $codekey.$filename;
            
            /*------------------------------------------*/
			$s3_region='sa-east-1';

            $s3_bucket='multiplierapp-form-data';
            $config = [
               's3' => [
                   'key' => 'AKIAUKMIEK7TCYNVUWPW',
                   'secret' => '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',
                   'bucket' => $s3_bucket
               ]
			];

            $s3 = S3Client::factory([
                'credentials' => [
                   'key' => $config['s3']['key'],
                   'secret' => $config['s3']['secret']
                ],
				'region' => $s3_region,
				'version' => 'latest'
            ]);
				
            $obj = $s3->putObject([
                            'Bucket'       => $config['s3']['bucket'],
                            'Key'          => 'user-videos/'.$filename,
                            'SourceFile'   => $_FILES['file']['tmp_name'],
                            'ACL'          => 'public-read',
                            'StorageClass' => 'REDUCED_REDUNDANCY'
					]);	
		}

		$ApplicationVideoTable = TableRegistry::get('ApplicationVideos');

		if( $_POST['video_editor_id'] ){
			$application_video = $ApplicationVideoTable->get( $_POST['video_editor_id'] );
		}
		else{
			$application_video = $ApplicationVideoTable->newEntity();
			$application_video->created = Time::now();
		}

		$application_video->application_id = $_POST['application_id'];
		$application_video->video_name =  ($application_video->video_name && is_null($filename) ) ? $application_video->video_name : $filename;
		$application_video->title = $_POST['title'];
		$application_video->video_date = $_POST['date'];
		$application_video->description = $_POST['description'];	

		$result = $ApplicationVideoTable->save($application_video);

		$data = [
			'video_name' => $filename,
			'title' => $_POST['title'],
			'date' => $_POST['date'],
			'description' => $_POST['description'],
			'id' => $result->id,
		];

			echo json_encode($data);die; 
	}

	/**
	 * Delete videos
	 */
	public function deleteVideoGallery($video_id){

		$ApplicationVideoTable = TableRegistry::get('ApplicationVideos');

		$application_video = $ApplicationVideoTable->get($video_id);

		$deleted = false;

		if($application_video){

			$ApplicationVideoTable->delete($application_video);

			$s3_region='sa-east-1';
            $s3_bucket='multiplierapp-form-data';
            $config = [
               's3' => [
                   'key' => 'AKIAUKMIEK7TCYNVUWPW',
                   'secret' => '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',
                   'bucket' => $s3_bucket
               ]
			];
			
            $s3 = S3Client::factory([
                'credentials' => [
                   'key' => $config['s3']['key'],
                   'secret' => $config['s3']['secret']
                ],
				'region' => $s3_region,
				'version' => 'latest'
            ]);

			$obj = $s3->deleteObject([
				'Bucket'  => $config['s3']['bucket'],
				'Key'     => "user-videos/".$application_video->video_name,
			]);
				
			$deleted = true;
		}

		echo $deleted;
		exit();
	}

	/**
	 * 
	 */
	public function loadVideoGallery($application_id, $start, $quantity){

		$ApplicationVideoTable = TableRegistry::get('ApplicationVideos');

		$application_videos = $ApplicationVideoTable->find()
			->select([
				"ApplicationVideos.id",
				"ApplicationVideos.video_name",
				"ApplicationVideos.title",
				"ApplicationVideos.description",
				"video_date_only" => "DATE_FORMAT(ApplicationVideos.video_date, '%d/%m/%Y')",
				"video_hours_watched" => "TIME(ApplicationVideos.hours_watched)"
			])
			->where(["ApplicationVideos.application_id" => $application_id])
			->order("ApplicationVideos.video_date DESC")
			->offset($start)
			->limit($quantity)
			->toArray();
		
		echo json_encode($application_videos);
		die();
	}

	/**
	 * 
	 */
	public function getTotalHoursWatched($application_id){

		$this->viewBuilder()->setLayout('admin_inner');

		$ApplicationVideoTable = TableRegistry::get('ApplicationVideos');

		$total_hours_watched = $ApplicationVideoTable->find()
			->select(["total_hours_watched" => "SEC_TO_TIME( SUM( TIME_TO_SEC(ApplicationVideos.hours_watched) ) )"])
			->where(["ApplicationVideos.application_id" => $application_id])
			->group("ApplicationVideos.application_id")
			->first();

		echo $total_hours_watched;
		die();
	}

}