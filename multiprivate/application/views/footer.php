<?php
    $link = $_SERVER['PHP_SELF'];
    $link_array = explode('/',$link);
    $page = end($link_array);

	$is_livestream = ($page === "live-stream") ? 1 : 0;
	$language = $_SESSION['livemode_multiplier']['language'];~

	$application_id = isset($formdata) ? $formdata->application_id : "";
?>

<?php if(isset($formdata) && !empty($formdata)){ ?>
	<div class="module-small bg-dark" id = "footer_content">
	<div class="container">
		<div class="row">
		<div class="widget">
			<h5 class="widget-title font-alt">&nbsp;&nbsp;&nbsp;<?php echo ($formdata->footer_title)?$formdata->footer_title:$this->lang->line('footer_title');?></h5>
			<p>&nbsp;&nbsp;&nbsp;<?php echo ($formdata->footer_sub_title)?$formdata->footer_sub_title:$this->lang->line('footer_sub_title');?></p>
			<?php  if($formdata->company_email){?>
				<p>&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('contact');?>:&nbsp;<a href="mailto:<?php echo $formdata->company_email;?>"><?php echo $formdata->company_email;?></a></p>
			<?php }?>
			
			<?php if ($is_livestream){ ?>
				<h3 id = "gallery_name" > Gallery </h3>
				<h5 id = "total_hours_watched">  </h5>
				<div class = "row" id = "video_gallery"></div>
			<?php } ?>

		</div>
		</div>
	</div>
	</div>


	<hr class="divider-d">
	<footer class="footer bg-dark">
	<div class="container">
		<div class="row">
		<div class="col-sm-6">
			<p class="copyright font-alt">&copy; 2018&nbsp;<a href="#"><?php echo ($formdata->company_name)?$formdata->company_name:$this->lang->line('company');?></a>, <?php echo $this->lang->line('rights');?></p>
		</div>
		<br>
		<br>
		<div class="col-sm-6">
			<div class="footer-social-links">
					<?php  if($formdata->facebook_url){?>
						<a href="<?php echo addhttp($formdata->facebook_url); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
					<?php }?>
					<?php  if($formdata->twitter_url){?>
						<a href="<?php echo addhttp($formdata->twitter_url); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
					<?php }?>
					<?php  if($formdata->website_url){?>
						<a href="<?php echo addhttp($formdata->website_url); ?>" target="_blank"><i class="fa fa-dribbble"></i></a>
					<?php }?>
					<?php  if($formdata->skype_url){?>
						<a href="skype:<?php echo $formdata->skype_url; ?>?chat" ><i class="fa fa-skype"></i></a>
					<?php }?>
					<?php  if($formdata->youtube_url){?>
						<a href="<?php echo addhttp($formdata->youtube_url); ?>" target="_blank"><i class="fa fa-youtube"></i></a>
					<?php }?>
					<?php  if($formdata->linkedIn_url){?>
						<a href="<?php echo addhttp($formdata->linkedIn_url); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
					<?php }?>
				</div>
		</div>
		</div>
	</div>
	</footer>
	</div>
<?php }?>
<div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
<!--
JavaScripts
=============================================
-->

<?php $var = "3.6.3" ?>
<script src='<?php echo base_url("assets/lib/jquery/dist/jquery.js"); ?>'></script>
<script src="<?php echo base_url("assets/newTheme/assets/js/vendors/vendors.js?v=").$var ?>"></script>
<!-- Local Revolution tools-->
<!-- Only for local and can be removed on server-->

<script src="<?php echo base_url("assets/newTheme/assets/js/custom.js?v=").$var ?>"></script>

<script src="<?php echo base_url("assets/newTheme/assets/js/jquery.playSound.js?v=").$var ?>"></script>

<script type="text/javascript" src="<?php echo base_url("assets/newTheme/assets/js/twemoji/2/twemoji.min.js?v=").$var ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/newTheme/assets/js/twemoji-picker.js?v=").$var ?>"></script>

<script type="text/javascript" src="<?php echo base_url("assets/newTheme/assets/js/loadingoverlay.js?v=").$var ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/newTheme/assets/js/loadingoverlay_progress.js?v=").$var ?>"></script>

<script src="<?php echo base_url("assets/newTheme/assets/js/magicsuggest.js?v=").$var ?>"></script>
<script src="<?php echo base_url("assets/newTheme/assets/js/perfect-scrollbar.jquery.min.js?v=").$var ?>"></script>
<script src='<?php echo base_url("assets/lib/bootstrap/dist/js/bootstrap.min.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/wow/dist/wow.js"); ?>'></script>
<script src='<?php echo base_url("assets/js/parsley.min.js"); ?>'></script>
<?php if(getClientSessionVariable('language')=='spanish'){ ?>
	<script src='<?php echo base_url("assets/js/i18n/es.js"); ?>'></script>
<?php }?>
<?php if(getClientSessionVariable('language')=='portuguese'){ ?>
	<script src='<?php echo base_url("assets/js/i18n/pt-br.js"); ?>'></script>
<?php }?>
<script src='<?php echo base_url("assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/isotope/dist/isotope.pkgd.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/imagesloaded/imagesloaded.pkgd.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/flexslider/jquery.flexslider.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/owl.carousel/dist/owl.carousel.min.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/smoothscroll.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/magnific-popup/dist/jquery.magnific-popup.js"); ?>'></script>
<script src='<?php echo base_url("assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"); ?>'></script>
<script src='<?php echo base_url("assets/js/plugins.js"); ?>'></script>
<script src='<?php echo base_url("assets/js/main.js"); ?>'></script>


<?php if($is_livestream){ ?>

	<script>

		/** Gallery JS */

		var application_id = <?= $application_id ?>;

		//Temporary vars for manual translation
		var language = "<?= $language ?>";

		var translation = {
			seeMore: {
				portuguese: "Ver Mais",
				english: "See More",
				spanish: "Ver Más",
			},
			seeLess: {
				portuguese: "Ver Menos",
				english: "See Less",
				spanish: "Ver Menos",
			},
			gallery: {
				portuguese: "Galeria",
				english: "Gallery",
				spanish: "Galería",
			},
			watched: {
				portuguese: "Horas Assistidas",
				english: "Hours Watched",
				spanish: "Horas Visto",
			},
			total_hours_watched: {
				portuguese: "Total de horas assistidas",
				english: "Total hours watched",
				spanish: "Total de horas vistas",
			}
		};

		// Default value for the quantity of new loaded videos when ending the scroll
		var load_quantity_end_scroll = 6;

		var start = 0;

		//Tells if there are no more itens to be loaded
		var no_more_itens = false;

		$(document).ready(function(){

			$("#gallery_name").text(translation.gallery[language]);
			getTotalHoursWatched();

			addNewVideosToGallery();

			$("#video_gallery").scroll(function () {

				var element = document.getElementById("video_gallery");

				if( scrolledToBottom(element) && !no_more_itens && $("#loader").length === 0 ){
					addNewVideosToGallery();
				}

			});

		});

		$(document).on('click', '.description-see-more', function(e){
			$(this).text(translation.seeLess[language]).removeClass("description-see-more").addClass("description-see-less").parent().children(".item-description").removeClass("truncate-text");
			e.preventDefault();
		});

		$(document).on('click', '.description-see-less', function(e){
			$(this).text(translation.seeMore[language]).removeClass("description-see-less").addClass("description-see-more").parent().children(".item-description").addClass("truncate-text");
			e.preventDefault();
		});

		function getTotalHoursWatched(){

			var url = `${window.location.origin}/app/webregistration/getTotalHoursWatched/${application_id}`;
			
			$.ajax({	
				url:url,
				method:"POST",
				contentType: false,
				cache: false,
				dataType: "text",
				processData: false,
				beforeSend:function(){
					addLoader();
				},
				success:function(dt) {
					data = JSON.parse(dt);

					if(data.length === 0){
						no_more_itens = true;
					}
					else{
						$("#total_hours_watched").text(translation.total_hours_watched[language] + " : " + data.total_hours_watched);
					}

				},
				complete: function() {
					removeLoader();
				}
			});
		}

		/**
		 * Load the itens in the gallery
		 */
		function loadGalleryItems(load_quantity){

			var url = `${window.location.origin}/app/webregistration/loadVideoGallery/${application_id}/${start}/${load_quantity}`;
			
			$.ajax({	
				url:url,
				method:"POST",
				contentType: false,
				cache: false,
				dataType: "text",
				processData: false,
				beforeSend:function(){
					addLoader();
				},
				success:function(dt) {
					data = JSON.parse(dt);

					if(data.length === 0){
						no_more_itens = true;
					}
					else{

						$.each(data, function(index, value){ 
							addVideo(value);
						});

						start += load_quantity;

						$(".item-title").tooltip()
					}

				},
				complete: function() {
					removeLoader();
				}
			});

		}

		/**
		 * Add a new video to the end of the gallery
		 */
		function addVideo(item){

			$("#video_gallery").append(`
				<div class = "gallery-item" data-id = ${item.id}>

					<video controls>
						<source src = "https://multiplierapp-form-data.s3-sa-east-1.amazonaws.com/user-videos/${item.video_name}">
					</video>

					<div class = "item-information">
						<h4 class = "item-title truncate-text" title = "${item.title}"> ${item.title} </h4>
						<div class = "item-description-wrapper">
							<span class = "item-description item-metadata truncate-text"> ${item.description} </span>
							<a class = "description-see-more" type = "button" href = "#">${translation.seeMore[language]}</a>
						</div>
						<span class = "item-date item-metadata"> ${item.video_date_only} </span>
						<span class = "item-watched-hours item-metadata"> ${translation.watched[language]}: ${item.video_hours_watched} </span>
						<hr class = "sm-divider">
					</div>

				</div>
			`);

		}

		/**
		 * Add news videos to the gallery when you reach the end of the scroll
		 */
		function addNewVideosToGallery(){
			loadGalleryItems(load_quantity_end_scroll);			
		}

		/**
		 * Checks if the element has been scrolled to the end
		 */
		function scrolledToBottom(element){
			return (element.offsetHeight + element.scrollTop >= element.scrollHeight - 20) ? true : false;
		}

		/**
		 * Add the loader 
		 */
		function addLoader(target){
			$("#video_gallery").append(`<div id="loader"></div>`);
		}

		/**
		 * Remove the loader
		 */
		function removeLoader(){
			$("#loader").remove();
		}
		
	</script>

	<style>

		html{
			overflow: inherit;
		}

		/** Footer */
		#footer_content{
			background-color: #282828 !important;
			padding: 25px 0;
		}

		.footer{
			background-color: #282828 !important;
		}

		/** Video Gallery */

		#gallery_name{
			color: #ffffff;
			border-bottom: 1px solid;
			display: inline-block;
			padding: 10px 20px 10px 0;
		}

		#video_gallery{
			display: flex;
			flex-direction: row;
			flex-wrap: wrap;
			justify-content: flex-start;
			max-height: 600px;
			overflow-y: scroll;
		}

		#video_gallery .gallery-item{
			flex: 1;
			flex-basis: 30%;
			margin: 1%;
			flex-grow: 0;
			position: relative;
		}

		#video_gallery .gallery-item video{
			width: 100%;
			height: 200px;
		}

		#video_gallery .item-information{
			display: grid;
			max-width: 85%;
		}

		#video_gallery .item-title{
			max-width: 400px;
			margin: 0;
			margin-bottom: 10px;
			width: 100%;
			max-height: 3.5em;	
			color: #ffffff;
			font-weight: bold;
			font-size: 1.2em;
			text-align: initial;
		}

		#video_gallery .item-metadata{
			text-align: initial;
			color: #c5c5c5;
			font-size: 0.85em;
			line-height: 2;
		}

		#video_gallery .item-description-wrapper{
			margin: 10px 0;
			text-align: initial;
		}

		#video_gallery .item-description{
			line-height: 1.6;
		}

		#video_gallery .description-see-more, #video_gallery .description-see-less{
			display: block;
			text-align: initial;
			text-decoration: underline;
			color: #58A4A0;
		}

		.sm-divider{
			display: none;
			margin-top: 20px;
		}

		/** Custom Scrollbar */

		#video_gallery::-webkit-scrollbar-track {
			border: 1px solid #000;
			padding: 2px 0;
			background-color: #404040;
		}

		#video_gallery::-webkit-scrollbar {
			width: 10px;
		}

		#video_gallery::-webkit-scrollbar-thumb {
			border-radius: 10px;
			box-shadow: inset 0 0 6px rgba(0,0,0,.3);
			background-color: #737272;
			border: 1px solid #000;
		}

		/** Truncate Text With Ellipsis  */

		.truncate-text{
			overflow: hidden;
			display: -webkit-box;
			-webkit-line-clamp: 3;
			-webkit-box-orient: vertical;
		}

		/** Loader */

		#loader {
			position: absolute;
			left: 50%;
			top: 50%;
			z-index: 1;
			margin: -75px 0 0 -75px;
			border: 5px solid #f3f3f3;
			border-radius: 50%;
			border-top: 5px solid #58A4A0;
			width: 100px;
			height: 100px;
			-webkit-animation: spin 2s linear infinite;
			animation: spin 2s linear infinite;
		}

		@-webkit-keyframes spin {
			0% { -webkit-transform: rotate(0deg); }
			100% { -webkit-transform: rotate(360deg); }
		}

		@keyframes spin {
			0% { transform: rotate(0deg); }
			100% { transform: rotate(360deg); }
		}

		/* Add animation to "page content" */
		.animate-bottom {
			position: relative;
			-webkit-animation-name: animatebottom;
			-webkit-animation-duration: 1s;
			animation-name: animatebottom;
			animation-duration: 1s
		}

		@-webkit-keyframes animatebottom {
			from { bottom:-100px; opacity:0 } 
			to { bottom:0px; opacity:1 }
		}

		@keyframes animatebottom { 
			from{ bottom:-100px; opacity:0 } 
			to{ bottom:0; opacity:1 }
		}

		@media(max-width: 1000px){

			#video_gallery .gallery-item {
				flex-basis: 45%;
			}			

		}

		@media(max-width: 650px){

			#video_gallery{
				justify-content: center;
			}

			#video_gallery .gallery-item {
				flex-basis: 90%;
				margin-bottom: 30px;
			}			

			.sm-divider{
				display: inherit;
			}
		}

	</style>

<?php } ?>




<style>
   .footer .footer-social-links a{
   	    font-size: 24px;
   }

   #message_twemoji{
    margin-left: -14px;
   }
</style>

</main>
</body>
</html>